"""
Set the variable as keys in dictionary so that they can be overwritten
by the cmd line easily.
"""
import random
import string
import torch

class AttributeDict(dict):
    __getattr__ = dict.__getitem__
    __setattr__ = dict.__setitem__


CONSTANTS = AttributeDict()

"""
Geometry constants.
"""
CONSTANTS.GEOMETRIES = ["SiW",]  # ["SiW", "SciPb"]
CONSTANTS.ORG_N_CELLS_Z = 45
CONSTANTS.ORG_N_CELLS_R = 18
CONSTANTS.N_CELLS_PHI = 50
CONSTANTS.SIZE_R = 2.325
CONSTANTS.SIZE_Z = 3.4

# In case of restricting data (Count from 0, Including)
CONSTANTS.R_HIGH = None  # [0, 17]
CONSTANTS.Z_LOW = None  # [0, 44]
CONSTANTS.Z_HIGH = None  # [0, 44]
if CONSTANTS.R_HIGH is None: CONSTANTS.R_HIGH = 17
if CONSTANTS.Z_LOW is None: CONSTANTS.Z_LOW = 0
if CONSTANTS.Z_HIGH is None: CONSTANTS.Z_HIGH = 44
assert CONSTANTS.R_HIGH < CONSTANTS.ORG_N_CELLS_R
assert (CONSTANTS.Z_HIGH - CONSTANTS.Z_LOW) < CONSTANTS.ORG_N_CELLS_Z
CONSTANTS.N_CELLS_R = CONSTANTS.R_HIGH + 1
CONSTANTS.N_CELLS_Z = CONSTANTS.Z_HIGH - CONSTANTS.Z_LOW + 1

# Minimum and maximum primary particle energy to consider for training in GeV units.
CONSTANTS.ENERGIES = [64, 128, 256]
CONSTANTS.MIN_ENERGY = min(CONSTANTS.ENERGIES)
CONSTANTS.MAX_ENERGY = max(CONSTANTS.ENERGIES)
# Minimum and maximum primary particle angle to consider for training in degrees units.
CONSTANTS.ANGLES = [70,]
CONSTANTS.MIN_ANGLE = min(CONSTANTS.ANGLES)
CONSTANTS.MAX_ANGLE = max(CONSTANTS.ANGLES)


"""
Directories.
"""
CONSTANTS.GLOBAL_CHECKPOINT_DIR = "./checkpoint"
CONSTANTS.CONV_DIR = "./conversion"
CONSTANTS.VALID_DIR = "./validation/" + ''.join(random.choices(string.ascii_lowercase, k=5))
CONSTANTS.GEN_DIR = "./generation"


"""
Model default parameters.
"""
CONSTANTS.MODEL_TYPE = 'ViTDiffusion'
CONSTANTS.BATCH_SIZE_PER_REPLICA = 256
CONSTANTS.ORIGINAL_DIM = CONSTANTS.N_CELLS_Z * CONSTANTS.N_CELLS_R * CONSTANTS.N_CELLS_PHI
CONSTANTS.EPOCHS = 5000
CONSTANTS.LEARNING_RATE = 0.001
CONSTANTS.VALIDATION_SPLIT = 0.10
CONSTANTS.OPTIMIZER_TYPE = torch.optim.Adam
CONSTANTS.SAVE_MODEL = False


"""
Managing GPUs
"""
# GPU identifiers separated by comma, no spaces.
CONSTANTS.GPU_IDS = "0"
# Maximum allowed memory on one of the GPUs (in GB)
CONSTANTS.MAX_GPU_MEMORY_ALLOCATION = 44  # Outdated


"""
AE/VAE/VQVAE Transformer parammeters
"""
CONSTANTS.NUM_LAYERS = 2
CONSTANTS.NUM_HEADS = 4
CONSTANTS.PROJECTION_DIM = 128
CONSTANTS.QKV_DIM = CONSTANTS.PROJECTION_DIM // CONSTANTS.NUM_HEADS
CONSTANTS.FF_DIM = 256
CONSTANTS.DROPOUT = 0.1
CONSTANTS.PATCH_R = 3
CONSTANTS.PATCH_PHI = 10
CONSTANTS.PATCH_Z = 15
CONSTANTS.ADD_CONDITIONS = False


"""
AE/VAE parameters.
"""
CONSTANTS.LATENT_DIM = 32 * CONSTANTS.PATCH_R * CONSTANTS.PATCH_PHI * CONSTANTS.PATCH_Z
CONSTANTS.KL_WEIGHT = 1


"""
Diffusion/Latent Diffusion Transformer parameters
"""
CONSTANTS.DF_NUM_LAYERS = 4
CONSTANTS.DF_NUM_HEADS = 8
CONSTANTS.DF_PROJECTION_DIM = 128
CONSTANTS.DF_QKV_DIM = CONSTANTS.DF_PROJECTION_DIM // CONSTANTS.DF_NUM_HEADS
CONSTANTS.DF_FF_DIM = 256
CONSTANTS.DF_DROPOUT = 0.1
CONSTANTS.CONDITIONS_AS_TOKEN = True
CONSTANTS.ASSIST_TOKEN = False
CONSTANTS.AE_CHECKPOINT = None
CONSTANTS.SCHEDULAR = 'cosine'
CONSTANTS.NOISE_STEPS = 400


"""
Validator parameter.
"""
CONSTANTS.FULL_SIM_HISTOGRAM_COLOR = "blue"
CONSTANTS.ML_SIM_HISTOGRAM_COLOR = "red"
CONSTANTS.FULL_SIM_GAUSSIAN_COLOR = "green"
CONSTANTS.ML_SIM_GAUSSIAN_COLOR = "orange"
CONSTANTS.HISTOGRAM_TYPE = "step"


"""
W&B parameters.
"""
# Change this to your entity name.
CONSTANTS.WANDB_ENTITY = "foundation-models"
CONSTANTS.PLOT_FREQ = 25
CONSTANTS.PLOT_CONFIG = [
    # [70, 64, 'SiW'],
    [70, 128, 'SiW'],
    # [70, 256, 'SiW'],
]  # List of [angle, energy, geometry]


"""
Experimental
"""
# More data
CONSTANTS.USE_MORE_DATA = False
# Directory to load the full simulation dataset.
if CONSTANTS.USE_MORE_DATA:
    CONSTANTS.INIT_DIR = "/eos/geant4/fastSim/Par04/HighStat/detector_SiW/"
else:
    CONSTANTS.INIT_DIR = "/eos/geant4/fastSim/Par04_public/HDF5_Zenodo/"
