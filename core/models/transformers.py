"""
Outdated except MHA implementation.

Attentions are not masked!
"""
import torch
import torch.nn as nn
import numpy as np
from einops.layers.torch import Rearrange as RearrangeEinops
from einops.einops import rearrange

from core.models.handler import ModelHandler
from core.constants import CONSTANTS

PATCH_R = CONSTANTS.PATCH_R
PATCH_PHI = CONSTANTS.PATCH_PHI
PATCH_Z = CONSTANTS.PATCH_Z


class MultiHeadAttention(nn.Module):
    def __init__(self, d_model, n_head, d_k, d_v, dropout=0.0):
        super().__init__()
        self.n_head = n_head

        self.w_qs = nn.Linear(d_model, n_head * d_k)
        self.w_ks = nn.Linear(d_model, n_head * d_k)
        self.w_vs = nn.Linear(d_model, n_head * d_v)

        nn.init.normal_(self.w_qs.weight, mean=0, std=np.sqrt(2.0 / (d_model + d_k)))
        nn.init.normal_(self.w_ks.weight, mean=0, std=np.sqrt(2.0 / (d_model + d_k)))
        nn.init.normal_(self.w_vs.weight, mean=0, std=np.sqrt(2.0 / (d_model + d_v)))

        self.fc = nn.Linear(n_head * d_v, d_model)
        self.dropout = nn.Dropout(dropout)
        nn.init.xavier_normal_(self.fc.weight)

    def forward(self, q, k, v, mask=None):
        q = rearrange(self.w_qs(q), 'b l (head k) -> head b l k', head=self.n_head)
        k = rearrange(self.w_ks(k), 'b t (head k) -> head b t k', head=self.n_head)
        v = rearrange(self.w_vs(v), 'b t (head v) -> head b t v', head=self.n_head)
        attn = torch.einsum('hblk,hbtk->hblt', [q, k]) / np.sqrt(q.shape[-1])
        if mask is not None:
            attn = attn.masked_fill(mask[None, :, None, :], -np.inf)  # [heads, batch, seq, seq]
        attn = torch.softmax(attn, dim=3)
        output = torch.einsum('hblt,hbtv->hblv', [attn, v])
        output = rearrange(output, 'head b l v -> b l (head v)')
        output = self.fc(output)
        return self.dropout(output), attn


class TransformerEncoderLayer(nn.Module):
    def __init__(self, projection_dim, num_heads, dim_feedforward=None, dropout=0.0, qkv_dim=None):
        super().__init__()
        if qkv_dim == None: qkv_dim = projection_dim // num_heads
        if dim_feedforward is None:
            dim_feedforward = projection_dim
        self.ln1 = nn.LayerNorm(projection_dim)
        self.attn = MultiHeadAttention(projection_dim, num_heads, qkv_dim, qkv_dim, dropout)
        self.ln2 = nn.LayerNorm(projection_dim)
        self.mlp = nn.Sequential(
            nn.Linear(projection_dim, dim_feedforward),
            nn.GELU(),
            nn.Dropout(dropout),
            nn.Linear(dim_feedforward, projection_dim),
            nn.Dropout(dropout)
        )

    def forward(self, inputs):
        x1 = self.ln1(inputs)
        x2 = self.attn(x1, x1, x1)[0]
        x3 = inputs + x2
        x4 = self.ln2(x3)
        x4 = self.mlp(x4)
        return x3 + x4


class TransformerDecoderLayer(nn.Module):
    def __init__(self, projection_dim, num_heads, dim_feedforward=None, dropout=0.0, qkv_dim=None):
        super().__init__()
        if dim_feedforward is None:
            dim_feedforward = projection_dim
        self.ln1 = nn.LayerNorm(projection_dim)
        self.attn = MultiHeadAttention(projection_dim, num_heads, qkv_dim, qkv_dim)
        self.ln2 = nn.LayerNorm(projection_dim)
        self.mlp = nn.Sequential(
            nn.Linear(projection_dim, dim_feedforward),
            nn.GELU(),
            nn.Dropout(dropout),
            nn.Linear(dim_feedforward, projection_dim)
        )

    def forward(self, inputs):
        x, attn_mask = inputs
        x1 = self.ln1(x)
        x2 = self.attn(x1, x1, x1, mask=attn_mask)[0]
        x3 = x + x2
        x4 = self.ln2(x3)
        x4 = self.mlp(x4)
        return x3 + x4, attn_mask  # Pass attn mask to next TransformerDecoderLayer


class HackLayer(nn.Module):
    def forward(self, x):
        return x[0]


def create_mask(idx):
    batch_size = idx.shape[0]
    mask = torch.zeros(batch_size, NUM_CODEBOOK_VECTORS_PER_IMAGE).bool().to(idx.device)
    # Dynamic slicing
    temp_idx = torch.arange(NUM_CODEBOOK_VECTORS_PER_IMAGE).to(idx.device)
    mask[temp_idx[None, :] >= idx[:, None]] = True
    return mask


class Fixed1DPositionalEmbeddings(nn.Module):
    """
    For nth codebook vector
    """
    def __init__(self, allotted_space):
        super().__init__()

        patches_dirn = [NUM_CODEBOOK_VECTORS_PER_IMAGE + (1 if CONDITIONS_AS_TOKEN else 0)]

        space_x = allotted_space
        # sin-cos, hence needs to be even number
        assert space_x % 2 == 0
        space_dirn = [space_x]

        # We have PATCH_X patches in each direction
        encodings = [torch.zeros(patches_dirn[i], space_dirn[i]) for i in range(1)]
        pos = [torch.arange(0, patches_dirn[i]).float().unsqueeze(1) for i in range(1)]
        _2i = [torch.arange(0, space_dirn[i], step=2).float() for i in range(1)]

        for i in range(1):
            encodings[i][:, 0::2] = torch.sin(pos[i] / (10000 ** (_2i[i] / space_dirn[i])))
            encodings[i][:, 1::2] = torch.cos(pos[i] / (10000 ** (_2i[i] / space_dirn[i])))

        self.encodings = RearrangeEinops("cv d -> 1 cv d")(encodings[0])
        self.encodings.requires_grad = False

    def forward(self):
        return self.encodings


class Fixed3DPositionalEmbeddings(nn.Module):
    def __init__(self, allotted_space):
        super().__init__()
        patches_dirn = [PATCH_R, PATCH_PHI, PATCH_Z]

        # Don't determine (for now) if encodings are need in each direction and how many
        # Simpler implementation
        space_x = allotted_space // 3
        # sin-cos, hence needs to be even number
        if space_x % 2 == 1: space_x += 1
        space_dirn = [space_x, space_x]
        space_dirn.append((allotted_space - sum(space_dirn))//2*2)

        # We have PATCH_X patches in each direction
        encodings = [torch.zeros(patches_dirn[i], space_dirn[i]) for i in range(3)]
        pos = [torch.arange(0, patches_dirn[i]).float().unsqueeze(1) for i in range(3)]
        _2i = [torch.arange(0, space_dirn[i], step=2).float() for i in range(3)]

        for i in range(3):
            encodings[i][:, 0::2] = torch.sin(pos[i] / (10000 ** (_2i[i] / space_dirn[i])))
            encodings[i][:, 1::2] = torch.cos(pos[i] / (10000 ** (_2i[i] / space_dirn[i])))

        # Repeat in non-matching dirn
        # r dirn
        encodings[0] = encodings[0].unsqueeze(1).unsqueeze(1).repeat(1, PATCH_PHI, PATCH_Z, 1)
        # phi dirn
        encodings[1] = encodings[1].unsqueeze(1).unsqueeze(0).repeat(PATCH_R, 1, PATCH_Z, 1)
        # z dirn
        encodings[2] = encodings[2].unsqueeze(0).unsqueeze(0).repeat(PATCH_R, PATCH_PHI, 1, 1)

        encodings = torch.cat(encodings, dim=-1)
        self.encodings = RearrangeEinops("r p z d -> 1 (r p z) d")(encodings)
        self.encodings.requires_grad = False

    def forward(self):
        return self.encodings