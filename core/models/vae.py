"""
Mostly outdated.
"""

import torch
import torch.nn as nn

from core.models.handler import ModelHandler
from core.constants import CONSTANTS


ORIGINAL_DIM = CONSTANTS.ORIGINAL_DIM
LATENT_DIM = CONSTANTS.LATENT_DIM
ACTIVATION = CONSTANTS.ACTIVATION
OUT_ACTIVATION = CONSTANTS.OUT_ACTIVATION
KERNEL_INITIALIZER = CONSTANTS.KERNEL_INITIALIZER
BIAS_INITIALIZER = CONSTANTS.BIAS_INITIALIZER
INTERMEDIATE_DIMS = CONSTANTS.INTERMEDIATE_DIMS
KL_WEIGHT = CONSTANTS.KL_WEIGHT


def _Sampling(z_mean, z_log_var):
    """ Custom layer to do the reparameterization trick: sample random latent vectors z from the latent Gaussian
    distribution.
    The sampled vector z is given by sampled_z = mean + std * epsilon
    """
    z_sigma = torch.exp(0.5 * z_log_var)
    return z_mean + z_sigma * torch.randn(z_mean.shape, device=z_mean.device)


# KL divergence computation
def _KLDivergence(mu, log_var, **kwargs):
    kl_loss = -0.5 * (1 + log_var - torch.square(mu) - torch.exp(log_var))
    kl_loss = kl_loss.mean(dim=-1).mean()
    # Note: could be kl_loss.sum(dim=-1).mean(), original paper sums over latent dim
    return kl_loss


class VAELoss:
    def __init__(self, model):
        self.model = model

    def __call__(self, y_pred, y_true):
        loss = nn.BCELoss(reduction='sum')(y_pred, y_true)
        loss += KL_WEIGHT * self.model.get_KL()
        return loss


class Encoder(nn.Module):
    def __init__(self, original_dim, intermediate_dims, latent_dim, activation, kernel_initializer, bias_initializer):
        super().__init__()
        all_dims = [original_dim + 4,] + intermediate_dims
        self.blocks = nn.Sequential(
            *[self.block(all_dims[i], all_dims[i+1], activation) for i in range(len(all_dims) - 1)]
        )
        self.mu_layer = nn.Linear(all_dims[-1], latent_dim)
        self.log_var_layer = nn.Linear(all_dims[-1], latent_dim)

    def block(self, in_dims, out_dims, activation):
        return nn.Sequential(
            nn.Linear(in_dims, out_dims),
            activation(),
            nn.BatchNorm1d(out_dims)
        )

    def forward(self, inputs):
        x = torch.concat(inputs, dim=1)
        for block in self.blocks:
            x = block(x)
        return self.mu_layer(x), self.log_var_layer(x)


class Decoder(nn.Module):
    def __init__(self, original_dim, intermediate_dims, latent_dim, activation, out_activation, kernel_initializer, bias_initializer):
        super().__init__()
        all_dims = [latent_dim + 4,] + intermediate_dims[::-1]
        self.blocks = nn.Sequential(
            *[self.block(all_dims[i], all_dims[i+1], activation) for i in range(len(all_dims) - 1)]
        )
        self.output_layer = nn.Sequential(
            nn.Linear(all_dims[-1], original_dim),
            out_activation()
        )

    def block(self, in_dims, out_dims, activation):
        return nn.Sequential(
            nn.Linear(in_dims, out_dims),
            activation(),
            nn.BatchNorm1d(out_dims)
        )

    def forward(self, inputs, inference=True):
        if inference:
            # replace showers w/ noise
            inputs[0] = torch.randn(inputs[0].shape[0], LATENT_DIM, device=inputs[0].device)
        x = torch.concat(inputs, dim=1)
        for block in self.blocks:
            x = block(x)
        return self.output_layer(x)


class VAE(nn.Module):
    def __init__(self, encoder, decoder, **kwargs):
        super(VAE, self).__init__(**kwargs)
        self.encoder = encoder
        self.decoder = decoder

    def forward(self, inputs):
        x, e_input, angle_input, geo_input = inputs
        self.mu, self.log_var = self.encoder([x, e_input, angle_input, geo_input])
        z = _Sampling(self.mu, self.log_var)
        return self.decoder([z, e_input, angle_input, geo_input], inference=False)

    def get_KL(self):
        return _KLDivergence(self.mu, self.log_var)


class VAEHandler(ModelHandler):
    def __init__(self, **kwargs):
        self._encoder_nw = Encoder(ORIGINAL_DIM, INTERMEDIATE_DIMS, LATENT_DIM, ACTIVATION, KERNEL_INITIALIZER, BIAS_INITIALIZER)
        self._decoder_nw = Decoder(ORIGINAL_DIM, INTERMEDIATE_DIMS, LATENT_DIM, ACTIVATION, OUT_ACTIVATION, KERNEL_INITIALIZER, BIAS_INITIALIZER)
        self._latent_dim = LATENT_DIM
        super().__init__(**kwargs)

    def _get_wandb_extra_config(self):
        return {
            "activation": ACTIVATION,
            "out_activation": OUT_ACTIVATION,
            "intermediate_dims": INTERMEDIATE_DIMS,
            "latent_dim": LATENT_DIM,
            "num_layers": len(INTERMEDIATE_DIMS)
        }

    def _initialize_model(self):
        self._model = VAE(self._encoder_nw, self._decoder_nw)
        self._loss = VAELoss(self._model)

    def _set_model_inference(self):
        self._decoder = self._decoder_nw
