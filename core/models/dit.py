import os
from functools import partialmethod
import torch
import torch.nn as nn
import numpy as np
from einops.layers.torch import Rearrange as RearrangeEinops
from einops.einops import rearrange
import torch.nn.functional as F
import wandb
import tqdm
from matplotlib import pyplot as plt

from core.models.handler import ModelHandler, ValidationPlotCallback
from core.constants import CONSTANTS
from core.models.transformers import MultiHeadAttention, TransformerEncoderLayer, Fixed3DPositionalEmbeddings
from core.models.diffusion import Diffusion, PositionalEmbeddingTth

tqdm.tqdm.__init__ = partialmethod(tqdm.tqdm.__init__, disable=True)
plt.rcParams.update({"font.size": 22})


# General constants
ORIGINAL_DIM = CONSTANTS.ORIGINAL_DIM
N_CELLS_R = CONSTANTS.N_CELLS_R
N_CELLS_PHI = CONSTANTS.N_CELLS_PHI
N_CELLS_Z = CONSTANTS.N_CELLS_Z
MAX_ENERGY = CONSTANTS.MAX_ENERGY
PLOT_CONFIG = CONSTANTS.PLOT_CONFIG
PLOT_FREQ = CONSTANTS.PLOT_FREQ

# Transformer constants
PATCH_R = CONSTANTS.PATCH_R
PATCH_PHI = CONSTANTS.PATCH_PHI
PATCH_Z = CONSTANTS.PATCH_Z
CONDITIONS_AS_TOKEN = CONSTANTS.CONDITIONS_AS_TOKEN
DF_NUM_LAYERS = CONSTANTS.DF_NUM_LAYERS
DF_NUM_HEADS = CONSTANTS.DF_NUM_HEADS
DF_DROPOUT = CONSTANTS.DF_DROPOUT
DF_PROJECTION_DIM = CONSTANTS.DF_PROJECTION_DIM
DF_QKV_DIM = CONSTANTS.DF_QKV_DIM
DF_FF_DIM = CONSTANTS.DF_FF_DIM
NOISE_STEPS = CONSTANTS.NOISE_STEPS
SCHEDULAR = CONSTANTS.SCHEDULAR
VALID_DIR = CONSTANTS.VALID_DIR


class DiTEncoderLayer(TransformerEncoderLayer):
    def forward(self, inputs):
        x_t, c = inputs
        # c = conditions - concat - timesteps
        x = torch.cat([x_t, c], dim=1)  # patches dim
        out = super().forward(x)
        out = out[:, :450, :]  # is this correct?

        # # add conditions to patches
        # x = x_t + c
        # out = super().forward(x)
        return (out, c)


class HackLayer(nn.Module):
    def forward(self, inputs):
        x_t, c = inputs
        return x_t


class TransformerDiffusion(nn.Module):
    """
    ViT style architecture
    """
    def __init__(self):
        super().__init__()
        self._num_patches = PATCH_R * PATCH_PHI * PATCH_Z
        self._feature_dim = (N_CELLS_R * N_CELLS_PHI * N_CELLS_Z) // self._num_patches

        self.position_encoding_layer = Fixed3DPositionalEmbeddings(DF_PROJECTION_DIM)
        self.time_encoding_layer = PositionalEmbeddingTth(DF_PROJECTION_DIM // 2)
        self.condition_token_projection = nn.Linear(1, DF_PROJECTION_DIM // 2)
        self.patchify = RearrangeEinops("b (i r) (j p) (k z) -> b (i j k) (r p z)", i=PATCH_R, j=PATCH_PHI, k=PATCH_Z)
        self.depatchify = RearrangeEinops("b (i j k) (r p z) -> b (i r) (j p) (k z)",
            i=PATCH_R, j=PATCH_PHI, k=PATCH_Z, r=N_CELLS_R//PATCH_R, p=N_CELLS_PHI//PATCH_PHI, z=N_CELLS_Z//PATCH_Z)

        self.projection_layer = nn.Linear(self._feature_dim, DF_PROJECTION_DIM)

        self.transformer_encoder = nn.Sequential(*([
            DiTEncoderLayer(
                DF_PROJECTION_DIM, DF_NUM_HEADS, dim_feedforward=DF_FF_DIM, dropout=DF_DROPOUT, qkv_dim=DF_QKV_DIM
            ) for i in range(DF_NUM_LAYERS)
            ] + [
            HackLayer(),
            nn.LayerNorm(DF_PROJECTION_DIM),
            nn.Linear(DF_PROJECTION_DIM, self._feature_dim),
            nn.GELU(),
            nn.Linear(self._feature_dim, self._feature_dim),
            # no sigmoid
        ]))

        time_dim = DF_PROJECTION_DIM // 2
        self.time_mlp = nn.Sequential(
            self.time_encoding_layer,
            nn.Linear(time_dim, time_dim),
            nn.GELU(),
            nn.Linear(time_dim, time_dim)
        )

    def forward(self, x_t, t, e_cond):
        # Reshaping from original dim
        x_t = x_t.view(x_t.shape[0], N_CELLS_R, N_CELLS_PHI, N_CELLS_Z)

        patches = self.patchify(x_t)
        patches = self.projection_layer(patches) + self.position_encoding_layer().to(patches.device)

        condn_projection = self.condition_token_projection(e_cond)
        time_projection = self.time_mlp(t)
        c = torch.cat([condn_projection, time_projection], dim=-1)
        c = c.view(c.shape[0], 1, c.shape[1])

        x = self.transformer_encoder((patches, c))
        x = self.depatchify(x)
        x = x.view(x.shape[0], -1)
        return x


class TransformerDiffusionHandler(ModelHandler):
    def _get_wandb_extra_config(self):
        return {
            "num_layers": DF_NUM_LAYERS,
            "num_heads": DF_NUM_HEADS,
            "projection_dim": DF_PROJECTION_DIM,
            "ff_dims": DF_FF_DIM,
            "dropout": DF_DROPOUT,
            "PATCH_R": PATCH_R,
            "PATCH_PHI": PATCH_PHI,
            "PATCH_Z": PATCH_Z
        }

    def _initialize_model(self):
        self._model = TransformerDiffusion()
        self._loss = nn.MSELoss(reduction='mean')
        self._diffusion = None
        self.EP = 0

    def _set_model_inference(self):
        self._decoder = self._model
    
    def _train_one_epoch(self, trainloader, validloader, optimizer, device):
        if self._diffusion is None:  # to init with the device
            self._diffusion = Diffusion(noise_steps=NOISE_STEPS, schedule=SCHEDULAR, device=device)

        self._model.train()
        train_loss_bce = 0.0
        for X, y in trainloader:
            for i in range(len(X)):
                X[i] = X[i].to(device)
            y = y.to(device)

            batch_size = y.shape[0]
            x, e_cond, a_cond, geo_cond = X

            # forward diffusion
            t = self._diffusion.sample_timesteps(batch_size)
            x_t, noise = self._diffusion.noise_images(x, t)

            # backward diffusion
            optimizer.zero_grad()
            y_hat = self._model(x_t, t, e_cond)
            loss = self._loss(y_hat, noise)
            loss.backward()
            optimizer.step()

            train_loss_bce += loss.item()

        self._model.eval()
        val_loss_bce = 0.0
        with torch.no_grad():
            for X, y in validloader:
                for i in range(len(X)):
                    X[i] = X[i].to(device)
                y = y.to(device)

                batch_size = y.shape[0]
                x, e_cond, a_cond, geo_cond = X

                # forward diffusion
                t = self._diffusion.sample_timesteps(batch_size)
                x_t, noise = self._diffusion.noise_images(x, t)

                # backward diffusion
                y_hat = self._model(x_t, t, e_cond)
                loss = self._loss(y_hat, noise)

                val_loss_bce += loss.item()

        train_loss_bce /= len(trainloader)
        val_loss_bce /= len(validloader)

        wandb.log({
            'train_loss_bce': train_loss_bce,
            'val_loss_bce': val_loss_bce,
        })

        # another validloader pass for known t
        if self.EP%25==0:
            self._model.eval()
            t_losses = []
            for _t in range(self._diffusion.noise_steps):
                val_loss_t = 0.0
                with torch.no_grad():
                    for X, y in validloader:
                        for i in range(len(X)):
                            X[i] = X[i].to(device)
                        y = y.to(device)

                        batch_size = y.shape[0]
                        x, e_cond, a_cond, geo_cond = X

                        # forward diffusion
                        t = torch.tensor([_t] * batch_size).to(device)
                        x_t, noise = self._diffusion.noise_images(x, t)

                        # backward diffusion
                        y_hat = self._model(x_t, t, e_cond)
                        loss = self._loss(y_hat, noise)

                        val_loss_t += loss.item()
                        break  # just for one batch

                t_losses.append(val_loss_t)

            self.plot_t_losses(t_losses)
        self.EP += 1

        return train_loss_bce, val_loss_bce

    def predict(self, dataloader, device):
        """
        Inference loop using self._decoder.
        """
        self._set_model_inference()
        self._decoder.to(device)
        self._decoder.eval()

        results = []
        i = 0 
        # Loop over batch
        for X, _ in dataloader:
            i += 1
            if i==11:
                break
            for i in range(len(X)):
                X[i] = X[i].to(device)

            e_cond = X[1]
            x_T = torch.randn_like(X[0])
            y_hat = self._diffusion.sample(self._decoder, x_T, e_cond)
            results.append(y_hat.detach().cpu().numpy())

        results = np.concatenate(results, axis=0)
        return results.reshape(results.shape[0], -1)

    def plot_t_losses(self, t_losses):
        plt.figure(figsize=(12, 8))
        plt.plot(range(self._diffusion.noise_steps), t_losses)
        plt.xlabel("t")
        plt.xlabel("loss")
        plt.title(f"Epoch: {self.EP} loss for each diffusion timestep")
        plt.savefig(f"{VALID_DIR}/Loss_diffusion_timestep.png")
        plt.clf()
        wandb.log({'t_losses': wandb.Image(f"{VALID_DIR}/Loss_diffusion_timestep.png")})
