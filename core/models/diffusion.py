import torch
import torch.nn as nn
import numpy as np
from einops.layers.torch import Rearrange as RearrangeEinops
from einops.einops import rearrange
import torch.nn.functional as F
import wandb
import tqdm
import math

from core.constants import CONSTANTS

from functools import partialmethod

tqdm.tqdm.__init__ = partialmethod(tqdm.tqdm.__init__, disable=True)


class PositionalEmbeddingTth(nn.Module):
    def __init__(self, dim):
        super().__init__()
        self.dim = dim

    def forward(self, x):
        device = x.device
        half_dim = self.dim // 2
        emb = math.log(10000) / (half_dim - 1)
        emb = torch.exp(torch.arange(half_dim, device=device) * -emb)
        emb = x[:, None] * emb[None, :]
        emb = torch.cat((emb.sin(), emb.cos()), dim=-1)
        return emb


class Diffusion:
    def __init__(self, noise_steps=400, img_size=(18, 50, 45), schedule='cosine', device="cuda"):
        self.noise_steps = noise_steps
        self.img_size = img_size #size of the output
        self.schedule = schedule
        self.device = device

        self.beta = self.prepare_noise_schedule(self.schedule).to(device)
        self.alpha = 1. - self.beta
        self.alpha_bar = torch.cumprod(self.alpha, dim=0)
        self.alpha_bar_prev = F.pad(self.alpha_bar[:-1], (1, 0), value=1.0)
        self.alphas_bar_sqrt = torch.sqrt(self.alpha_bar)
        self.one_minus_alphas_bar_sqrt = torch.sqrt(1 - self.alpha_bar)
        self.posterior_variance = self.beta * (1. - self.alpha_bar_prev) / (1. - self.alpha_bar)

    def prepare_noise_schedule(self, schedule='cosine'):
        if schedule=='linear':
            return self.linear_beta_schedule(self.noise_steps)
        elif schedule=='cosine':
            return self.cosine_beta_schedule(self.noise_steps)

    def linear_beta_schedule(self, timesteps, beta_start=1e-4, beta_end=0.02):
        scale = 1000 / timesteps
        beta_start = scale * 0.0001
        beta_end = scale * 0.02
        return torch.linspace(beta_start, beta_end, timesteps)

    def cosine_beta_schedule(self, timesteps, s=0.008):
        steps = timesteps + 1
        x = torch.linspace(0, timesteps, steps)
        alphas_cumprod = torch.cos(((x / timesteps) + s) / (1 + s) * torch.pi * 0.5) ** 2
        alphas_cumprod = alphas_cumprod / alphas_cumprod[0]
        betas = 1 - (alphas_cumprod[1:] / alphas_cumprod[:-1])
        return torch.clip(betas, 0.0001, 0.9999)

    def extract(self, vals, t, x):
        # extract value from vals in the x shape
        x_shape = x.shape
        out = torch.gather(vals, -1, t.to(self.device))
        reshape_shape = [t.shape[0]] + [1] * (len(x_shape) - 1)
        return out.reshape(*reshape_shape)

    def noise_images(self, x, t, noise=None):
        # for forward diffusion process
        if noise is None:
            noise = torch.randn_like(x).to(x.device)
        alphas_t = self.extract(self.alphas_bar_sqrt, t, x)
        alphas_1_m_t = self.extract(self.one_minus_alphas_bar_sqrt, t, x)
        return (alphas_t * x + alphas_1_m_t * noise), noise

    def sample_timesteps(self, n):
        return torch.randint(low=1, high=self.noise_steps, size=(n,)).to(self.device)

    def sample(self, model, x_T, e_cond):
        with torch.no_grad():
            x_t = x_T
            x_t = x_t.view(x_t.shape[0], -1)
            for i in reversed(range(0, self.noise_steps)):
                t = (torch.ones(x_t.shape[0]) * i).long().to(self.device)
                predicted_noise = model(x_t, t, e_cond)
                alpha = self.extract(self.alpha, t, x_t) 
                alpha_bar = self.extract(self.alpha_bar, t, x_t) 
                beta = self.extract(self.beta, t, x_t)
                mean = 1 / torch.sqrt(alpha) * (x_t - ((1 - alpha) / (torch.sqrt(1 - alpha_bar))) * predicted_noise)
                if i == 0:
                    return mean
                else:
                    noise = torch.randn_like(x_t)
                    posterior_variance = self.extract(self.posterior_variance, t, x_t)
                    return mean + torch.sqrt(posterior_variance) * noise 
        return x_t
