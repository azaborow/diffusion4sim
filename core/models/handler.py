import os
from dataclasses import dataclass

import numpy as np
import torch
import torch.nn as nn
import wandb
import plotly.graph_objects as go

from core import models
from validate import validate
from generate import generate
from utils.preprocess import load_showers, get_condition_arrays, preprocess_util
from core.models.data import getShowersDataloaders
from core.constants import CONSTANTS
from core.models.data import ShowersDataset
from validate import validate
from generate import generate


BATCH_SIZE = CONSTANTS.BATCH_SIZE_PER_REPLICA
EPOCHS = CONSTANTS.EPOCHS
LEARNING_RATE = CONSTANTS.LEARNING_RATE
OPTIMIZER_TYPE = CONSTANTS.OPTIMIZER_TYPE
GLOBAL_CHECKPOINT_DIR = CONSTANTS.GLOBAL_CHECKPOINT_DIR
SAVE_MODEL = CONSTANTS.SAVE_MODEL
WANDB_ENTITY = CONSTANTS.WANDB_ENTITY
INIT_DIR = CONSTANTS.INIT_DIR
VALID_DIR = CONSTANTS.VALID_DIR
VALIDATION_SPLIT = CONSTANTS.VALIDATION_SPLIT
PLOT_FREQ = CONSTANTS.PLOT_FREQ
PLOT_CONFIG = CONSTANTS.PLOT_CONFIG
N_CELLS_R = CONSTANTS.N_CELLS_R
N_CELLS_PHI = CONSTANTS.N_CELLS_PHI
N_CELLS_Z = CONSTANTS.N_CELLS_Z


def ResolveModel(model_type):
    if model_type=='VAE':
        return models.VAEHandler
    if model_type=='TransformerVAE':
        return models.TransformerVAE
    if model_type=='ViTDiffusion':
        return models.TransformerDiffusionHandler
    else:
        raise ValueError


@dataclass
class ModelHandler:
    """
    Class to handle building and training of models.
    """
    _wandb_run_name: str = None
    _wandb_project_name: str = None
    _log_to_wandb: bool = False
    _batch_size: int = BATCH_SIZE
    _learning_rate: float = LEARNING_RATE
    _epochs: int = EPOCHS
    _optimizer_type = OPTIMIZER_TYPE
    _checkpoint_dir: str = GLOBAL_CHECKPOINT_DIR
    _save_model: bool = SAVE_MODEL
    _validation_split: float = VALIDATION_SPLIT

    _model = None
    _decoder = None
    _loss = None

    def __post_init__(self) -> None:
        # Setup Wandb.
        if self._log_to_wandb:
            self._setup_wandb()

        self._initialize_model()
        self._save_dir = f"{self._checkpoint_dir}/{self._wandb_project_name}/{self._wandb_run_name}"

    def _setup_wandb(self) -> None:
        config = {
            "learning_rate": self._learning_rate,
            "batch_size": self._batch_size,
            "epochs": self._epochs,
            "optimizer_type": str(self._optimizer_type)
        }
        # Add model specific config
        config.update(self._get_wandb_extra_config())
        # Reinit flag is needed for hyperparameter tuning. Whenever new training is started, new Wandb run should be created.
        wandb.init(name=self._wandb_run_name, project=self._wandb_project_name, entity=WANDB_ENTITY, reinit=True, config=config)
        # Upload constants.py
        wandb.save("./core/constants.py")

    def _get_wandb_extra_config(self):
        raise NotImplementedError

    def _initialize_model(self):
        """
        Builds a model. Defines self._model. Should be defined by the inherited class.
        """
        raise NotImplementedError

    def _set_model_inference(self):
        """
        Inference pipeline. Defines self._decoder. Should be defined by the inherited class.
        """
        raise NotImplementedError

    def _get_dataloaders(self, dataset, e_cond, angle_cond, geo_cond):
        """
        Returns dataloaders for training. Should be defined by the inherited class.
        """
        return getShowersDataloaders(dataset, e_cond, angle_cond, geo_cond)

    def _train_one_epoch(self, trainloader, validloader, optimizer, device):
        self._model.train()
        train_loss = 0.0
        for X, y in trainloader:
            for i in range(len(X)):
                X[i] = X[i].to(device)
            y = y.to(device)

            optimizer.zero_grad()
            y_hat = self._model(X)
            loss = self._loss(y_hat, y)
            loss.backward()
            optimizer.step()

            train_loss += loss.item()

        self._model.eval()
        val_loss = 0.0
        with torch.no_grad():
            for X, y in validloader:
                for i in range(len(X)):
                    X[i] = X[i].to(device)
                y = y.to(device)

                y_hat = self._model(X)
                loss = self._loss(y_hat, y)

                val_loss += loss.item()

        return train_loss / len(trainloader), val_loss / len(validloader)

    
    def train_model(self, dataset, energy, angle, geo, device):
        trainloader, validloader = self._get_dataloaders(dataset, energy, angle, geo)

        self._model.to(device)
        self._device = device

        optimizer = self._optimizer_type(self._model.parameters(), self._learning_rate)

        scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
            optimizer, 'min', factor=0.5, min_lr=1e-4, patience=100, verbose=True)

        wandb.watch(self._model, log_freq=100, log='all')
        shower_observables_callbacks = []
        for _angle, _energy, _geo in PLOT_CONFIG:
            shower_observables_callbacks.append(
                ValidationPlotCallback(PLOT_FREQ, self, _angle, _energy, _geo, device)
            )

        # # Shower observables (untrained)
        # for so_config in shower_observables_callbacks:
        #     so_config(0)

        val_loss_min = float('inf')
        for epoch in range(self._epochs):
            # One epoch
            train_loss, val_loss = self._train_one_epoch(trainloader, validloader, optimizer, device)
            scheduler.step(val_loss)

            print("Epoch {}:\tTrainLoss: {} \tValidLoss: {}".format(epoch + 1, train_loss, val_loss))

            # WandB logs
            wandb.log({
                'train_loss': train_loss,
                'val_loss': val_loss,
                'epoch': epoch
            })

            # Save model if improvement
            if val_loss_min > val_loss:
                print("Saving model..")
                val_loss_min = val_loss
                if self._save_model:
                    self.save_model()
            
            # # save model anyway every 20 epochs
            # if self._save_model and epoch % 20 == 0:
            #     os.makedirs(self._save_dir, exist_ok=True)
            #     torch.save(self._model.state_dict(), f"{self._save_dir}/model_weights_{epoch}.pt")

            # Shower observables
            for so_config in shower_observables_callbacks:
                so_config(epoch)

    def predict(self, dataloader, device):
        """
        Inference loop using self._decoder.
        """
        self._set_model_inference()
        self._decoder.to(device)
        self._decoder.eval()

        results = []
        for X, _ in dataloader:
            for i in range(len(X)):
                X[i] = X[i].to(device)
            with torch.no_grad():
                y_hat = self._decoder(X)
            results.append(y_hat)

        results = torch.cat(results, dim=0)
        return results.detach().cpu().numpy()

    def save_model(self):
        os.makedirs(self._save_dir, exist_ok=True)
        torch.save(self._model.state_dict(), f"{self._save_dir}/model_weights.pt")

    def load_model(self, epoch=None):
        if epoch is None:
            epoch = ""
        else:
            epoch = f"_{epoch}"
        self._model.load_state_dict(torch.load(f"{self._save_dir}/model_weights{epoch}.pt"))


class ValidationPlotCallback:
    def __init__(self, verbose, handler, angle, energy, geometry, device):
        self.val_angle = angle
        self.val_energy = energy
        self.val_geometry = geometry
        self.handler = handler
        self.verbose = verbose
        self.device = device
        self._setup()

    def _setup(self):
        self.showers = load_showers(INIT_DIR, self.val_geometry, self.val_energy, self.val_angle)
        energy, angle, geo = get_condition_arrays(self.val_geometry, self.val_energy, self.val_angle, self.showers.shape[0])
        data = [preprocess_util(self.showers, self.val_energy),] + [energy, angle, geo]
        dataset = ShowersDataset(data)
        self.dataloader = torch.utils.data.DataLoader(dataset, batch_size=BATCH_SIZE, num_workers=4)

    def __call__(self, epoch):
        if (epoch % self.verbose)==0:
            print('Plotting..')
            self.handler._set_model_inference()
            generated_events = generate(self.handler, self.dataloader, self.val_energy, self.device)
            validate(self.showers, generated_events, self.val_energy, self.val_angle, self.val_geometry)

            observable_names = ["LatProf", "LongProf", "PhiProf", "E_tot", "E_tot_inc", "E_cell", "E_cell_non_log", "E_cell_non_log_xlog",
                "E_layer", "LatFirstMoment", "LatSecondMoment", "LongFirstMoment", "LongSecondMoment", "Radial_num_zeroes"]
            plot_names = [
                f"{VALID_DIR}/{metric}_Geo_{self.val_geometry}_E_{self.val_energy}_Angle_{self.val_angle}"
                for metric in observable_names
            ]
            for file in plot_names:
                wandb.log({file: wandb.Image(f"{file}.png")})

            # 3D shower
            shower = generated_events[0].reshape(N_CELLS_R, N_CELLS_PHI, N_CELLS_Z)
            r, phi, z, inn = np.stack([x.ravel() for x in np.mgrid[:N_CELLS_R, :N_CELLS_PHI, :N_CELLS_Z]] + [shower.ravel(),], axis=1).T
            phi = phi / phi.max() * 2 * np.pi
            x = r * np.cos(phi)
            y = r * np.sin(phi)

            normalize_intensity_by = 30  # knob for transparency
            trace = go.Scatter3d(
                x=x,
                y=y,
                z=z,
                mode='markers',
                marker_symbol='square',
                marker_color=[f"rgba(0,0,255,{i*100//normalize_intensity_by/100})" for i in inn],
            )
            go.Figure(trace).write_html(f"{VALID_DIR}/3d_shower.html")
            wandb.log({'shower_{}_{}'.format(self.val_angle, self.val_energy): wandb.Html(f"{VALID_DIR}/3d_shower.html")})
