


class ARV3VQVAETransformer(nn.Module):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._num_patches = PATCH_R * PATCH_PHI * PATCH_Z
        self._feature_dim = (N_CELLS_R * N_CELLS_PHI * N_CELLS_Z) // self._num_patches

        self.position_encoding_layer = Fixed3DPositionalEmbeddings(PROJECTION_DIM)
        self.patchify = RearrangeEinops("b (i r) (j p) (k z) -> b (i j k) (r p z)", i=PATCH_R, j=PATCH_PHI, k=PATCH_Z)
        self.depatchify = RearrangeEinops("b (i j k) (r p z) -> b (i r) (j p) (k z)",
            i=PATCH_R, j=PATCH_PHI, k=PATCH_Z, r=N_CELLS_R//PATCH_R, p=N_CELLS_PHI//PATCH_PHI, z=N_CELLS_Z//PATCH_Z)

        self.projection_layer = nn.Linear(self._feature_dim, PROJECTION_DIM)

        self.transformer_encoder = nn.Sequential(*([
            TransformerEncoderLayer(
                PROJECTION_DIM, NUM_HEADS, dim_feedforward=FF_DIM, dropout=DROPOUT, qkv_dim=QKV_DIM
            ) for i in range(NUM_LAYERS)
            ] + [
            nn.LayerNorm(PROJECTION_DIM),
        ]))

        self.transformer_decoder = nn.Sequential(*([
            TransformerEncoderLayer(
                PROJECTION_DIM, NUM_HEADS, dim_feedforward=FF_DIM, dropout=DROPOUT, qkv_dim=QKV_DIM
            ) for i in range(NUM_LAYERS)
        ] + [
            nn.LayerNorm(PROJECTION_DIM),
            nn.Linear(PROJECTION_DIM, self._feature_dim)
        ]))

        # self.codebook = VQEmbedding(VOCAB_SIZE, CODEBOOK_VECTOR_SIZE, data_init=DATA_INIT)
        self.codebook = VQEmbeddingEMA(VOCAB_SIZE, CODEBOOK_VECTOR_SIZE, data_init=DATA_INIT)
        # self.codebook = GumbelQuantize(VOCAB_SIZE, CODEBOOK_VECTOR_SIZE)
        # self.codebook = VAEEmbedding()
        # self.codebook = NSVQ(VOCAB_SIZE, CODEBOOK_VECTOR_SIZE, data_init=DATA_INIT)
        # self.codebook = KMeansVQ(VOCAB_SIZE, CODEBOOK_VECTOR_SIZE)
        self.threshold = StraightThroughEstimator()

    def forward(self, inputs, epoch=None):
        # Reshaping from original dim
        x, e_cond, a_cond, geo_cond = inputs
        x = x.view(x.shape[0], N_CELLS_R, N_CELLS_PHI, N_CELLS_Z)

        # pre-transformer steps
        patches = self.patchify(x)
        x = self.projection_layer(patches) + self.position_encoding_layer().to(patches.device)

        # VQVAE
        z_e_x = self.transformer_encoder(x)
        commit_loss, vq_loss, assign_loss, z_q_x, perplexity, _ = self.codebook(z_e_x, epoch)
        y_hat = self.transformer_decoder(z_q_x)

        # post-transformer steps
        y_hat = self.depatchify(y_hat)
        y_hat = F.leaky_relu(y_hat.view(y_hat.shape[0], -1))

        if THRESH_AWARE:
            y_hat = self.threshold(y_hat * e_cond) / e_cond

        return y_hat, z_e_x, z_q_x, perplexity, commit_loss, vq_loss, assign_loss, _
    
    def encoder(self, x):
        x = x.view(x.shape[0], N_CELLS_R, N_CELLS_PHI, N_CELLS_Z)

        # pre-transformer steps
        patches = self.patchify(x)
        extra_embeddings = self.position_encoding_layer()
        if self._add_conditions:
            extra_embeddings = torch.cat([extra_embeddings.repeat(x.shape[0], 1, 1), self.conditional_embedding_layer(e_cond, a_cond, geo_cond)], dim=-1)
        x = self.projection_layer(patches) + extra_embeddings.to(patches.device)

        # VQVAE
        enc_out = self.transformer_encoder(x)
        z_e_x = self.codebook.proj(enc_out)
        return z_e_x  # b d n
    
    def decoder(self, x):
        # x = b d n
        dec_in = self.codebook.deproj(x)
        y_hat = self.transformer_decoder(dec_in)

        # post-transformer steps
        y_hat = self.depatchify(y_hat)
        # No sigmoid here as ARV3 handler is applying it (Outdated)
        return y_hat

EP = 0
flag = False

class ARV3VQVAEHandler(ModelHandler):
    def _get_wandb_extra_config(self):
        return {
            "vocab_size": VOCAB_SIZE,
            "codebook_vector_size": CODEBOOK_VECTOR_SIZE,
            "num_codebook_vectors_per_image": NUM_CODEBOOK_VECTORS_PER_IMAGE,
        }

    def _initialize_model(self):
        self._model = ARV3VQVAETransformer()
        self._loss = nn.MSELoss(reduction='mean')

    def _set_model_inference(self):
        self._decoder = self._model

    def _train_one_epoch(self, trainloader, validloader, optimizer, device):
        """
        Train loop for VQ-VAE.
        """
        global EP
        EP += 1
        self._model.train()
        train_loss = 0.0
        train_loss_bce = 0.0
        train_loss_vq = 0.0
        train_loss_commit = 0.0
        train_loss_assign = 0.0
        for X, y in trainloader:
            for i in range(len(X)):
                X[i] = X[i].to(device)
            y = y.to(device)

            optimizer.zero_grad()
            global flag
            if flag:
                # import pdb;pdb.set_trace()
                pass
            y_hat, z_e_x, z_q_x, train_perplexity, loss_commit, loss_vq, loss_assign, idx = self._model(X, EP)

            # Loss
            loss_bce = self._loss(y_hat, y)
            loss = loss_bce + COEFF_COMMITMENT * loss_commit + COEFF_VQ * loss_vq + COEFF_ENTROPY_ASSIGNMENT * loss_assign
            loss.backward()
            optimizer.step()

            train_loss += loss.item()
            train_loss_bce += loss_bce.item()
            train_loss_vq += loss_vq.item()
            train_loss_commit += loss_commit.item()
            train_loss_assign += loss_assign.item()

        if EP%10==0:
            pass # self.set_loss_weights(trainloader, device)

        self._model.eval()
        val_loss = 0.0
        val_loss_bce = 0.0
        val_loss_vq = 0.0
        val_loss_commit = 0.0
        val_loss_assign = 0.0
        with torch.no_grad():
            for X, y in validloader:
                for i in range(len(X)):
                    X[i] = X[i].to(device)
                y = y.to(device)

                y_hat, z_e_x, z_q_x, val_perplexity, loss_commit, loss_vq, loss_assign, _ = self._model(X, EP)

                loss_bce = self._loss(y_hat, y)
                loss = loss_bce + COEFF_COMMITMENT * loss_commit + COEFF_VQ * loss_vq + COEFF_ENTROPY_ASSIGNMENT * loss_assign

                val_loss += loss.item()
                val_loss_bce += loss_bce.item()
                val_loss_vq += loss_vq.item()
                val_loss_commit += loss_commit.item()
                val_loss_assign += loss_assign.item()

        train_loss_bce /= len(trainloader)
        train_loss_vq /= len(trainloader)
        train_loss_commit /= len(trainloader)
        train_loss_assign /= len(trainloader)

        val_loss_bce /= len(validloader)
        val_loss_vq /= len(validloader)
        val_loss_commit /= len(validloader)
        val_loss_assign /= len(validloader)

        if val_loss_bce < 0.0044:
            flag = True

        wandb.log({
            'train_loss_bce': train_loss_bce,
            'train_loss_vq': train_loss_vq,
            'train_loss_commit': train_loss_commit,
            'train_perplexity': train_perplexity,
            'train_entropy_assign': -train_loss_assign,
            'val_loss_bce': val_loss_bce,
            'val_loss_vq': val_loss_vq,
            'val_loss_commit': val_loss_commit,
            'val_perplexity': val_perplexity,
            'val_entropy_assign': -val_loss_assign
        })

        return train_loss_bce, val_loss_bce

    # Outdated
    def set_loss_weights(self, trainloader, device):
        global COEFF_COMMITMENT, COEFF_VQ

        self._model.train()
        train_loss_bce = 0.0
        train_loss_vq = 0.0
        train_loss_commit = 0.0

        for X, y in trainloader:
            for i in range(len(X)):
                X[i] = X[i].to(device)
            y = y.to(device)

            with torch.no_grad():
                y_hat, z_e_x, z_q_x, train_perplexity, loss_commit, loss_vq, loss_assign, idx = self._model(X)

            # Loss
            loss_bce = self._loss(y_hat, y)

            train_loss_bce += loss_bce.item()
            train_loss_vq += loss_vq.item()
            train_loss_commit += loss_commit.item()

        train_loss_bce /= len(trainloader)
        train_loss_vq /= len(trainloader)
        train_loss_commit /= len(trainloader)

        # set weights
        COEFF_COMMITMENT = 0.25 * train_loss_commit / train_loss_bce
        COEFF_VQ = train_loss_vq / train_loss_bce
        print("weights changed", COEFF_COMMITMENT, COEFF_VQ)

    def predict(self, dataloader, device):
        """
        Inference loop using self._decoder.
        """
        self._set_model_inference()
        self._decoder.to(device)
        self._decoder.eval()

        results = []
        # Loop over batch
        for X, _ in dataloader:
            for i in range(len(X)):
                X[i] = X[i].to(device)

            X[0] = X[0].view(-1, ORIGINAL_DIM)

            y_hat, *_  = self._decoder(X)
            results.append(y_hat.detach().cpu().numpy())

        results = np.concatenate(results, axis=0)
        return results.reshape(results.shape[0], -1)
