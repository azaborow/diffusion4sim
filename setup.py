"""
** setup **
creates necessary folders
"""

import os

from core.constants import CONSTANTS

# Overriding to create base ./validation
CONSTANTS.VALID_DIR = "./validation"

for folder in [CONSTANTS.INIT_DIR,  # Directory to load the full simulation dataset
            #    CONSTANTS.GLOBAL_CHECKPOINT_DIR,  # Directory to save model checkpoints
               CONSTANTS.CONV_DIR,  # Directory to save model after conversion to a format that can be used in C++
               CONSTANTS.VALID_DIR,  # Directory to save validation plots
               CONSTANTS.GEN_DIR,  # Directory to save model generated showers
               ]:
    os.system(f"mkdir -p {folder}")
