import os
import argparse
import numpy as np

from core.constants import CONSTANTS
from utils.observables import LongitudinalProfile, LateralProfile, Energy, PhiProfile
from utils.plotters import ProfilePlotter, EnergyPlotter
from utils.preprocess import load_showers


INIT_DIR = CONSTANTS.INIT_DIR
GEN_DIR = CONSTANTS.GEN_DIR
N_CELLS_PHI = CONSTANTS.N_CELLS_PHI
N_CELLS_R = CONSTANTS.N_CELLS_R
N_CELLS_Z = CONSTANTS.N_CELLS_Z
VALID_DIR = CONSTANTS.VALID_DIR


def parse_args():
    p = argparse.ArgumentParser()
    p.add_argument("--geometry", type=str, default="")
    p.add_argument("--energy", type=int, default="")
    p.add_argument("--angle", type=int, default="")
    args = p.parse_args()
    return args


def validate(e_layer_g4, e_layer_model, particle_energy, particle_angle, geometry):
    # Reshape the events into 3D
    e_layer_model = e_layer_model.reshape((len(e_layer_model), N_CELLS_R, N_CELLS_PHI, N_CELLS_Z))

    # 3. Create observables from raw data.
    full_sim_long = LongitudinalProfile(_input=e_layer_g4)
    full_sim_lat = LateralProfile(_input=e_layer_g4)
    full_sim_phi = PhiProfile(_input=e_layer_g4)
    full_sim_energy = Energy(_input=e_layer_g4, _energy=particle_energy)
    ml_sim_long = LongitudinalProfile(_input=e_layer_model)
    ml_sim_lat = LateralProfile(_input=e_layer_model)
    ml_sim_phi = PhiProfile(_input=e_layer_model)
    ml_sim_energy = Energy(_input=e_layer_model, _energy=particle_energy)

    # 4. Plot observables
    longitudinal_profile_plotter = ProfilePlotter(particle_energy, particle_angle, geometry, full_sim_long, ml_sim_long,
                                                  _plot_gaussian=False)
    lateral_profile_plotter = ProfilePlotter(particle_energy, particle_angle,
                                             geometry, full_sim_lat, ml_sim_lat, _plot_gaussian=False)
    phi_profile_plotter = ProfilePlotter(particle_energy, particle_angle,
                                             geometry, full_sim_phi, ml_sim_phi, _plot_gaussian=False)
    energy_plotter = EnergyPlotter(particle_energy, particle_angle, geometry, full_sim_energy, ml_sim_energy)

    longitudinal_profile_plotter.plot_and_save()
    lateral_profile_plotter.plot_and_save()
    phi_profile_plotter.plot_and_save()
    energy_plotter.plot_and_save()


# main function
def main():
    # Parse commandline arguments
    args = parse_args()
    particle_energy = args.energy
    particle_angle = args.angle
    geometry = args.geometry
    # 1. Full simulation data loading
    # Load energy of showers from a single geometry, energy and angle
    e_layer_g4 = load_showers(INIT_DIR, geometry, particle_energy,
                              particle_angle)
    # 2. Fast simulation data loading, scaling to original energy range & reshaping
    e_layer_vae = np.load(f"{GEN_DIR}/Geo_{geometry}_E_{particle_energy}_Angle_{particle_angle}.npy")
    os.system(f"mkdir -p {VALID_DIR}")

    print("Data has been loaded.")
    validate(e_layer_g4, e_layer_vae, particle_energy, particle_angle, geometry)
    print("Done.")
    print(f"Results are saved in {VALID_DIR}")


if __name__ == "__main__":
    exit(main())
