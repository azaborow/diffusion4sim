import os
import h5py
import pickle
import numpy as np
import torch

from core.constants import CONSTANTS


INIT_DIR = CONSTANTS.INIT_DIR
ORIGINAL_DIM = CONSTANTS.ORIGINAL_DIM
MAX_ENERGY = CONSTANTS.MAX_ENERGY
MAX_ANGLE = CONSTANTS.MAX_ANGLE
GEOMETRIES = CONSTANTS.GEOMETRIES
R_HIGH = CONSTANTS.R_HIGH
Z_LOW = CONSTANTS.Z_LOW
Z_HIGH = CONSTANTS.Z_HIGH
ENERGIES = CONSTANTS.ENERGIES
ANGLES = CONSTANTS.ANGLES
USE_MORE_DATA = CONSTANTS.USE_MORE_DATA


## MEANs and STDs

# 3 energies, 1 angle
# # after scaling by E
# MEAN = 0.0006356458498034909
# SIGMA = 0.003142269363503746

# # w/o scaling by E
# MEAN = 0.09016303779042045
# SIGMA = 0.4419626749353605

# # w/o scaling by E and after log(x + 1e-8)
# MEAN = -16.07292694637262
# SIGMA = 5.92982403742295

# w/o scaling by E and log1p
MEAN = 0.05576043050988125
SIGMA = 0.20199285587099322


def preprocess_util(voxels, energy_particle, **kwargs):
    voxels = voxels
    # import pdb; pdb.set_trace()
    # eps = voxels[voxels!=0].min() / 2
    # voxels = np.clip(voxels, 0, 1-eps)
    voxels = np.log(voxels + 1)
    voxels = (voxels - MEAN) / SIGMA
    return voxels


def postprocess_util(voxels, energy_particle, **kwargs):
    global MEAN, SIGMA
    voxels = voxels * SIGMA + MEAN
    voxels = np.minimum(np.maximum(-1 + 1e-6, voxels), 11)
    voxels = np.exp(voxels) - 1
    voxels = np.maximum(voxels, 0)
    voxels = voxels
    return voxels


# preprocess function loads the data and returns the array of the shower energies and the condition arrays
def preprocess():
    energies_train = []
    cond_e_train = []
    cond_angle_train = []
    cond_geo_train = []
    if not USE_MORE_DATA:
        # Using data that is on Zenodo
        for geo in GEOMETRIES:
            dir_geo = INIT_DIR + geo + "/"
            for angle_particle in ANGLES:
                f_name = f"{geo}_angle_{angle_particle}.h5"
                f_name = dir_geo + f_name
                h5 = h5py.File(f_name, "r")
                for energy_particle in ENERGIES:
                    events = np.array(h5[f"{energy_particle}"])
                    num_events = events.shape[0]
                    events = preprocess_util(events, energy_particle)

                    # For restricted geometry
                    events = events[:, :(R_HIGH + 1), :, Z_LOW:(Z_HIGH + 1)]

                    energies_train.append(events.reshape(num_events, -1))

                    # Bring the conditions b/w [0,1]
                    cond_e_train.append([energy_particle / MAX_ENERGY] * num_events)
                    cond_angle_train.append([angle_particle / MAX_ANGLE] * num_events)
                    # build the geometry condition vector (1 hot encoding vector)
                    if geo == "SiW":
                        cond_geo_train.append([[0, 1]] * num_events)
                    if geo == "SciPb":
                        cond_geo_train.append([[1, 0]] * num_events)
    else:
        # Currently data is available for only one geometry, one angle and one energy in muliple chunks
        dir_geo = INIT_DIR
        angle_particle = 90
        geo = 'SiW'
        for energy_particle in ENERGIES:
            for fno in range(10):
                fname = f"SiW_E_{energy_particle}GeV_Angle_90_{fno}.h5"
                h5 = h5py.File(dir_geo + fname, "r")
                try:
                    events = np.array(h5[f"{energy_particle}GeV"])
                except:
                    events = np.array(h5[f"{energy_particle}"])
                events = preprocess_util(events, energy_particle)
                num_events = events.shape[0]

                # For restricted geometry
                events = events[:, :(R_HIGH + 1), :, Z_LOW:(Z_HIGH + 1)]

                energies_train.append(events.reshape(num_events, -1))

                # Bring the conditions b/w [0,1]
                cond_e_train.append([energy_particle / MAX_ENERGY] * num_events)
                cond_angle_train.append([angle_particle / MAX_ANGLE] * num_events)
                # build the geometry condition vector (1 hot encoding vector)
                if geo == "SiW":
                    cond_geo_train.append([[0, 1]] * num_events)
                if geo == "SciPb":
                    cond_geo_train.append([[1, 0]] * num_events)

    # return numpy arrays
    energies_train = np.concatenate(energies_train)
    cond_e_train = np.concatenate(cond_e_train)
    cond_angle_train = np.concatenate(cond_angle_train)
    cond_geo_train = np.concatenate(cond_geo_train)
    return energies_train, cond_e_train, cond_angle_train, cond_geo_train


# get_condition_arrays function returns condition values from a single geometry, a single energy and angle of primary particles
"""
    - geo : name of the calorimeter geometry (eg: SiW, SciPb)
    - energy_particle : energy of the primary particle in GeV units
    - nb_events : number of events
"""
def get_condition_arrays(geo, energy_particle, angle_particle, nb_events):
    cond_e = [energy_particle / MAX_ENERGY] * nb_events
    cond_angle = [angle_particle / MAX_ANGLE] * nb_events
    if geo == "SiW":
        cond_geo = [[0, 1]] * nb_events
    else:  # geo == "SciPb"
        cond_geo = [[1, 0]] * nb_events
    cond_e = np.array(cond_e)
    cond_angle = np.array(cond_angle)
    cond_geo = np.array(cond_geo)
    return cond_e, cond_angle, cond_geo


# load_showers function loads events from a single geometry, a single energy and angle of primary particles
"""
    - init_dir: the name of the directory which contains the HDF5 files 
    - geo : name of the calorimeter geometry (eg: SiW, SciPb)
    - energy_particle : energy of the primary particle in GeV units
    - angle_particle : angle of the primary particle in degrees
"""


def load_showers(init_dir, geo, energy_particle, angle_particle):
    if not USE_MORE_DATA:
        dir_geo = init_dir + geo + "/"
        f_name = f"{geo}_angle_{angle_particle}.h5"
        f_name = dir_geo + f_name
        # read the HDF5 file
        h5 = h5py.File(f_name, "r")
        energies = np.array(h5[f"{energy_particle}"])
    else:
        dir_geo = init_dir
        f_name = dir_geo + f"SiW_E_{energy_particle}GeV_Angle_90_0.h5"
        h5 = h5py.File(f_name, "r")
        energies = np.array(h5[f"{energy_particle}GeV"])

    # For restricted geometry
    energies = energies[:, :(R_HIGH + 1), :, Z_LOW:(Z_HIGH + 1)]
    return energies
