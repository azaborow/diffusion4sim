import torch
import seaborn as sns
import numpy as np
from scipy.stats import gaussian_kde
import matplotlib.pyplot as plt

from utils.preprocess import preprocess
from core.models.diffusion import Diffusion

showers, *_ = preprocess()
showers = showers[np.random.randint(0, showers.shape[0], (1000,))]

diff = Diffusion(noise_steps=200, schedule='cosine')

for t in range(200):
    noised = diff.noise_images(torch.tensor(showers).cuda(), torch.tensor([t] * showers.shape[0]))[0]
    density = gaussian_kde(noised.cpu().numpy().reshape(-1)) 
    x = np.arange(-1, 1, 0.01)
    y = density(x)
    plt.plot(x, y)
    plt.savefig(f"plots/forward_200_cosine_{t}.png")
    plt.close()
    # plot = sns.kdeplot(noised.cpu().numpy())
    # plot.figure.savefig(f"plots/forward_{t}.png")

